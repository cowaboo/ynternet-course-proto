---
title: 'Societé en métamorphose'
keyconcepts:
    'Société en métamorphose': 'http://netizen3.org/index.php/Soci%C3%A9t%C3%A9_en_m%C3%A9tamorphose'
    'Autre concept': 'http://netizen3.org/index.php/Soci%C3%A9t%C3%A9_en_m%C3%A9tamorphose'
---

Aujourd'hui, nous changeons de civilisation et nous entrons dans un nouveau paradigme aux dénominations les plus variées : monde fini, ère numérique, société de la connaissance, société de l'information...

**Laissez-nous construire la société de l'information**
<iframe width="560" height="315" src="https://www.youtube.com/embed/gPdxLSG4Emc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

 &nbsp;
 
 La marque la plus visible de tout nouveau paradigme, c'est l'évolution de certaines croyances fondamentales.